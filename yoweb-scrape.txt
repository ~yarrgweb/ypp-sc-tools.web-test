		yoweb-scrape
		------------

This program can:
  * Fetch information for crew and pirates from yoweb
  * Display tables of puzzle standings
  * Keep track of who is on board a vessel

Putting this all together, it can be used to show a table of who's
good at what when you're running a pillage.

You run it roughly like this:
  ./yoweb-scrape ship-aid ~/yohoho/Aristarchus_midnight_chat-log-iwj 
It has various other modes - see the usage message.


Key features:

 * Tracks boarding and departure of pirates and shows a table
   of currently-aboard pirates with their relevant puzzle standings in
   a compressed format.

 * Shows time and nature their last interaction with you as XO
   (abandoning stations, being given orders) and where and when their
   last to-you-visible chat was.

 * Remembers whether you have ever given the pirate an order to gun
   since they boarded.

 * Caches and severely rate-limits fetches from yoweb to avoid
   overloading the Three Rings servers.  In ship-aid mode, will
   display twirling batons while you wait for data, so that crew
   on board info remains correct.

 * Can sort pirates by their skills in the key duty puzzles.  Press
   a single keystroke to sort by:
      s: sailing                   b: bilging
      c: carpentry                 g: gunning
      n: navigation (duty nav)     t: treasure haul
      d: driving (battle nav)      a: sort by pirate name
   It will prefer to list near the top pirates whose other skills
   are weaker.  This functionality is disabled if stdin is not a tty.
   Other keystrokes:
      q: quit the program (^C works too)
      /: start entering a local extra commands (see below)
      +: start entering local extra commands starting "/a"
      ESC: clear local extra command or abandon local extra commands entry

Things you need to know:

 * Please don't leave it running if you're not in charge.  If everyone
   runs yoweb-scrape all the time it will produce precisely the load
   on the yoweb servers that we want to avoid.

 * You must enable the YPP client chat log.  It is disabled by
   default.  This can be found in the client in the options panel
   under `Ye'.  Contrary to what the client UI seems to suggest, the
   filename you specify there is not used directly: information about
   your pirate name and ocean are added by the client, which is good
   because yoweb-scrape can dig it out again.

 * If you leave and then reboard a vessel, the list of pirates on
   board may be wrong.  This is because the chat log doesn't contain
   information about who's on board the ship when you reboard it.

   yoweb-scrape assumes that, unless it is told otherwise (or notices
   for itself), the set of pirates on board doesn't change while
   you're off the ship (unless you're away for more than an hour, in
   which case it assumes the ship is empty when you return to it).

   You can tell it otherwise with specially formatted chat messages.
   Messages sent or received to officer chat, and messages sent (not
   received) in private message, are scanned for special `commands' to
   yoweb-scrape.  (So you can have an officer on board the ship keep
   your yoweb-scrape up to date if you like.)
   
   The commands are of the following format:
         /a <pirate>...			/d <pirate>...
	 /a <ship-spec>: <pirate>...	/d <ship-spec>: <pirate>...
   And also:
         /j <pirate>
   which simulates "<pirate> applied for the posted job".

   <ship-spec> can be the full name of the ship, or or only the second
   word (the noun).  It can also be a pattern, where `*' matches any
   (nonempty) series of characters including spaces, and which counts
   if it matches either the whole ship name or just the noun.

   For example:
         /a haddock: aiah
	 /d aiah
	 /a smart sunfish: copperkatie
	 /a s* sunfish: anaplian
   These are actually sent by typing in the chat box things like:
         /o /d plonker
         /off /a hypiscles
   since other formulations interpret the scraper command as a YPP
   chat command.  This is deliberate and makes it harder to invoke by
   mistake.

   If you don't specify a ship, it will assume that it's the ship that
   you (or the officer speaking) are on board.  (For full details of
   the matching algorithm, see below.)  If more than one ship is being
   managed with yoweb-scrape, any user who wishes to issue these
   commands should say:
         /a relevant fish: executiveofficer
   so that everyone else's yoweb-scrape knows which ship they're on
   and thus which ship their /a commands refer to.

 * For the ship duty puzzle display, and for sorting pirates by
   skill, we take the best of Sailing and Rigging.

Other things to mention:

 * There are some things which don't reliably appear in the chat log,
   for example: when you disembark, and when a pirate leaves a duty
   station.  That can mean that yoweb-scrape isn't always as up to
   date as you would like.  You'll just have to tell it manually.

 * The `tabulate details of my whole crew' mode takes a long time
   to run.  This is normal; it's trying to spread the load on yoweb
   (perhaps excessively so).

 * I haven't decided on a final copyright licence.  You may treat this
   version as GPLv3.

 * The program will become confused when the clocks change,
   particularly when they go backwards.  This is due to the chat log
   containing timestamps in local time without a timezone.  If this
   bothers you, make everything be in UTC.  If it does get confused,
   you can fix it by logging off and logging on again (perhaps waiting
   until after the duplicated hour has passed) as that writes a new
   date record to the log and starts a new series of timestamps.

 * It's Python so it may eat CPU and memory.

 * You may need to install some python modules.  BeautifulSoup (yes,
   really!) is in the Debian package  python-beautifulsoup


Some more details about tracking semantics:

 * The program will notice that a pirate is on board if you give them
   a stationing order or they abandon a duty station (in circumstances
   where the game reports this in the log, which doesn't seem to be
   all the time), and from many of the other messages that occur.

 * Matching for ship patterns in /a and /d commands picks the first
   of the following which applies and is not contradicted by any
   name or partial name specified in the command:
      - The ship we think the person telling us is on board
      - The ship we think we are on board
      - Any other ship we know about, if at least a something was
         specified, and provided that is not ambiguous
      - The specific ship name if it is completely specified,
         possibly recording this as a new ship
   <pirate> and <ship> can be in any case and will be converted to
   the proper mixed-case form automatically.
