/*
 * Interaction with the YPP client via X11
 */
/*
 *  This is part of ypp-sc-tools, a set of third-party tools for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

/*
 * Only this file #includes the X11 headers, as they're quite
 * pollutant of the namespace.
 */

#include "structure.h"

#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>
#include <X11/keysym.h>
#include <X11/Xutil.h>

#include <X11/extensions/XShm.h>
#include <sys/ipc.h>
#include <sys/shm.h>

const char *ocean, *pirate;

static XWindowAttributes attr;
static Window id;
static Display *disp;
static struct timeval tv_startup;
static unsigned wwidth, wheight;
static int max_relevant_y= -1;
static Point commod_focus_point, commod_page_point, commod_focuslast_point;

static XImage *shmim;
static XShmSegmentInfo shminfo;

DEBUG_DEFINE_DEBUGF(pages)
DEBUG_DEFINE_SOME_DEBUGF(keymap,keydebugf)

#define xassert(what)					\
  ((what) ? (void)0 :					\
   fatal("X11 operation unexpectedly failed."		\
	 " %s:%d: %s\n", __FILE__,__LINE__,#what))


/*---------- keyboard map ----------*/

#define KEYS(EACH_KEY)				\
  EACH_KEY(XK_slash)				\
  EACH_KEY(XK_w)				\
  EACH_KEY(XK_Return)				\
  EACH_KEY(XK_Prior)				\
  EACH_KEY(XK_Next)

#define MAXMAPCOL 6
typedef struct {
  int len;
  int kc[3];
} MappedKey;

#define KEY_MAP_DEF(xk) \
static MappedKey mk_##xk;
KEYS(KEY_MAP_DEF)

typedef struct {
  int isdef; /* 'y' if defined; otherwise char indicating why not */
  int kc;
} MappedModifier;

static int kc_min, kc_max;
static MappedModifier mm_shift, mm_modeswitch, mm_isol3shift;
static KeySym *mapping;
static int mapwidth;

static void keymap_lookup_modifier(MappedModifier *mm, const char *what,
				   KeySym sym, char ifnot) {
  int kc;
  keydebugf("KEYMAP modifier lookup %-10s %#lx or '%c' ", what,
	    (unsigned long)sym, ifnot);

  mm->isdef= ifnot;
  for (kc=kc_min; kc <= kc_max; kc++) {
    if (mapping[(kc-kc_min)*mapwidth] == sym) {
      keydebugf(" found kc=%d\n", kc);
      mm->isdef= 'y';
      mm->kc= kc;
      return;
    }
  }
  keydebugf(" none\n");
}

static void keymap_lookup_key(MappedKey *mk, KeySym sym, const char *what) {
  const MappedModifier *shift, *xshift;
  int col, kc;
  char cols[MAXMAPCOL+1];

  memset(cols,'.',MAXMAPCOL);
  cols[MAXMAPCOL]= 0;

  for (col=0; col<mapwidth && col<MAXMAPCOL; col++) {
    keydebugf("KEYMAP lookup %-10s %#lx col=%d ",
	      what, (unsigned long)sym, col);

#define CHECK_SHIFT(sh) do{					\
      if ((sh) && (sh)->isdef!='y') {				\
	cols[col]= (sh)->isdef;					\
        keydebugf("no-modifier " #sh "'%c'\n", (sh)->isdef);	\
	continue;						\
      }								\
    }while(0)

    shift= col & 1 ? &mm_shift : 0;
    CHECK_SHIFT(shift);

    xshift= col >= 4 ? &mm_isol3shift :
            col >= 2 ? &mm_modeswitch :
            0;
    CHECK_SHIFT(xshift);

    for (kc=kc_min; kc <= kc_max; kc++) {
      if (mapping[(kc-kc_min)*mapwidth + col] == sym)
	goto found;
    }
    cols[col]= '_';
    keydebugf("not-found\n");
  }
  fprintf(stderr,"\n"
	  "Unable to find a key to press to generate %s.\n"
	  "(Technical details: cols:%s sh:%c:%d modesw:%c:%d isol3:%c:%d)\n",
	  what, cols,
#define SH_DETAILS(sh) mm_##sh.isdef, mm_##sh.kc
	  SH_DETAILS(shift), SH_DETAILS(modeswitch), SH_DETAILS(isol3shift));
  fatal("Keymap is unsuitable!");

 found:;
  int *fill= mk->kc;
  
  if (xshift) *fill++ = xshift->kc;
  if (shift)  *fill++ = shift->kc;
  *fill++ += kc;
  mk->len= fill - mk->kc;
  keydebugf("found kc=%d len=%d\n",kc,mk->len);
}

static void keymap_startup(void) {
  xassert( XDisplayKeycodes(disp,&kc_min,&kc_max) );
  keydebugf("KEYMAP keycodes %d..%d\n",kc_min,kc_max);

  xassert( mapping= XGetKeyboardMapping(disp, kc_min, kc_max-kc_min+1,
					&mapwidth) );
  keydebugf("KEYMAP got keyboard map\n");

  XModifierKeymap *modmap;
  xassert( modmap= XGetModifierMapping(disp) );
  keydebugf("KEYMAP got modifier map\n");

  /* find a shift keycode */
  mm_shift.isdef= 'x';
  int modent;
  for (modent=0; modent<modmap->max_keypermod; modent++) {
    int kc= modmap->modifiermap[modent];
    keydebugf("KEYMAP modifier #0 key #%d is %d ", modent, kc);
    if (!kc) { keydebugf("none\n"); continue; }
    keydebugf("ok\n");
    mm_shift.kc= kc;
    mm_shift.isdef= 'y';
    break;
  }

  XFreeModifiermap(modmap);

  /* find keycodes for mode_switch (column+=2) and ISO L3 shift (column+=4) */
  keymap_lookup_modifier(&mm_modeswitch,"modeswitch", XK_Mode_switch,     'm');
  keymap_lookup_modifier(&mm_isol3shift,"isol3shift", XK_ISO_Level3_Shift,'l');
  
#define KEY_MAP_LOOKUP(xk) \
  keymap_lookup_key(&mk_##xk, xk, #xk);
  KEYS(KEY_MAP_LOOKUP)

  XFree(mapping);
  mapping= 0;
}

#define SEND_KEY(xk) (send_key(&mk_##xk))

/*---------- pager ----------*/

typedef RgbImage Snapshot;

static double last_input;
static const double min_update_allowance= 0.25;

static double timestamp(void) {
  struct timeval tv;
  
  sysassert(! gettimeofday(&tv,0) );
  double t= (tv.tv_sec - tv_startup.tv_sec) +
            (tv.tv_usec - tv_startup.tv_usec) * 1e-6;
  debugf("PAGING %f\n",t);
  return t;
}
static void delay(double need_sleep) {
  debugf("PAGING     delay %f\n",need_sleep);
  sysassert(! usleep(need_sleep * 1e6) );
}

static void sync_after_input(void) {
  xassert( XSync(disp, False) );
  last_input= timestamp();
}

static void translate_coords_toroot(int wx, int wy, int *rx, int *ry) {
  Window dummy;
  xassert( XTranslateCoordinates(disp, id,attr.root, wx,wy, rx,ry, &dummy) );
}
static void translate_coords_toroot_p(Point w, int *rx, int *ry) {
  translate_coords_toroot(w.x, w.y, rx, ry);
}

static void check_client_window_all_on_screen(void) {
  Rect onroot;
  unsigned rwidth, rheight;
  Window dummy;
  unsigned bd, depth;
  int rxpos, rypos;

  xassert( XGetGeometry(disp,attr.root, &dummy, &rxpos,&rypos,
			&rwidth, &rheight,
			&bd,&depth) );
  
  translate_coords_toroot(0,0, &onroot.tl.x,&onroot.tl.y);
  translate_coords_toroot(wwidth-1,wheight-1, &onroot.br.x,&onroot.br.y);
  if (!(onroot.tl.x >= 0 &&
	onroot.tl.y >= 0 &&
	onroot.br.x < rwidth &&
	onroot.br.y < rheight))
    fatal("YPP client window is not entirely on the screen.");
}

static void check_not_disturbed(void) {
  XEvent ev;
  int r;
  
  for (;;) {
    r= XCheckMaskEvent(disp, ~0, &ev);
    if (r==False) return;

    switch (ev.type) {
    case VisibilityNotify:
      if (ev.xvisibility.state != VisibilityUnobscured)
	fatal("YPP client window has become obscured.");
      break;
    case ConfigureNotify:
      check_client_window_all_on_screen();
      break;
    case FocusOut:
      fatal("Focus left YPP client window.");
      break;
    case FocusIn:
      warning("focus entered YPP client window ?!");
      break;
    default:
      fatal("Received unexpected X11 event (type code %d)!", ev.type);
    }
  }
}      

static void check_pointer_not_disturbed(void) {
  Window got_root, got_child;
  int got_root_x, got_root_y;
  int got_win_x, got_win_y;
  unsigned got_mask;

  int r= XQueryPointer(disp,id, &got_root,&got_child,
		       &got_root_x, &got_root_y,
		       &got_win_x, &got_win_y,
		       &got_mask);
  if (!r ||
      got_win_x!=commod_page_point.x ||
      got_win_y!=commod_page_point.y) {
    progress("");
    fprintf(stderr,"\nunexpected mouse position:"
	    " samescreen=%d got=%dx%d want=%dx%d",
	    r, got_win_x,got_win_y,
	    commod_page_point.x,commod_page_point.y);
    fatal("Mouse pointer moved.");
  }
}

static void send_key(MappedKey *mk) {
  int i;
  check_not_disturbed();
  keydebugf("KEYMAP SEND_KEY len=%d kc=%d\n", mk->len, mk->kc[mk->len-1]);
  for (i=0; i<mk->len; i++) XTestFakeKeyEvent(disp, mk->kc[i], 1, 0);
  for (i=mk->len; --i>=0; ) XTestFakeKeyEvent(disp, mk->kc[i], 0, 0);
}
static void send_mouse_1_updown_here(void) {
  check_not_disturbed();
  XTestFakeButtonEvent(disp,1,1, 0);
  XTestFakeButtonEvent(disp,1,0, 0);
}
static void send_mouse_1_updown(int x, int y) {
  check_not_disturbed();
  int screen= XScreenNumberOfScreen(attr.screen);
  int xpos, ypos;
  translate_coords_toroot(x,y, &xpos,&ypos);
  XTestFakeMotionEvent(disp, screen, xpos,ypos, 0);
  send_mouse_1_updown_here();
}
static void pgdown_by_mouse(void) {
  check_not_disturbed();
  check_pointer_not_disturbed();
  debugf("PAGING   Mouse\n");
  send_mouse_1_updown_here();
  sync_after_input();
}

static int pgupdown;

static void send_pgup_many(void) {
  int i;
  for (i=0; i<25; i++) {
    SEND_KEY(XK_Prior);
    pgupdown--;
  }
  debugf("PAGING   PageUp x %d\n",i);
  sync_after_input();
}
static void send_pgdown_torestore(void) {
  debugf("PAGING   PageDown x %d\n", -pgupdown);
  while (pgupdown < 0) {
    SEND_KEY(XK_Next);
    pgupdown++;
  }
  sync_after_input();
}

static void free_snapshot(Snapshot **io) {
  free(*io);
  *io= 0;
}

#define SAMPLEMASK 0xfful

typedef struct {
  int lshift, rshift;
} ShMask;

static void compute_shift_mask(ShMask *sm, unsigned long ximage_mask) {
  sm->lshift= 0;
  sm->rshift= 0;
  
  for (;;) {
    if (ximage_mask <= (SAMPLEMASK>>1)) {
      sm->lshift++;  ximage_mask <<= 1;
    } else if (ximage_mask > SAMPLEMASK) {
      sm->rshift++;  ximage_mask >>= 1;
    } else {
      break;
    }
    assert(!(sm->lshift && sm->rshift));
  }
  assert(sm->lshift < LONG_BIT);
  assert(sm->rshift < LONG_BIT);
  debugf("SHIFTMASK %p={.lshift=%d, .rshift=%d} image_mask=%lx\n",
	 sm, sm->lshift, sm->rshift, ximage_mask);
}

static void rtimestamp(double *t, const char *wh) {
  double n= timestamp();
  debugf("PAGING                INTERVAL %f  %s\n", n-*t, wh);
  *t= n;
}

static void snapshot(Snapshot **output) {
  XImage *im_use, *im_free=0;

  ShMask shiftmasks[3];

  debugf("PAGING   snapshot\n");

  double begin= timestamp();
  if (shmim) {
    rtimestamp(&begin, "XShmGetImage before");
    xassert( XShmGetImage(disp,id,shmim, 0,0, AllPlanes) );
    rtimestamp(&begin, "XShmGetImage");

    size_t dsz= shmim->bytes_per_line * shmim->height;
    im_use= im_free= mmalloc(sizeof(*im_use) + dsz);
    *im_free= *shmim;
    im_free->data= (void*)(im_free+1);
    memcpy(im_free->data, shmim->data, dsz);
    rtimestamp(&begin, "mmalloc/memcpy");
  } else {
    rtimestamp(&begin, "XGetImage before");
    xassert( im_use= im_free=
	     XGetImage(disp,id, 0,0, wwidth,wheight, AllPlanes, ZPixmap) );
    rtimestamp(&begin, "XGetImage");
  }

#define COMPUTE_SHIFT_MASK(ix, rgb) \
  compute_shift_mask(&shiftmasks[ix], im_use->rgb##_mask)
  COMPUTE_SHIFT_MASK(0, blue);
  COMPUTE_SHIFT_MASK(1, green);
  COMPUTE_SHIFT_MASK(2, red);
  
  if (!*output)
    *output= alloc_rgb_image(wwidth, wheight);

  rtimestamp(&begin, "compute_shift_masks+alloc_rgb_image");

  int x,y,i;
  uint32_t *op32= (*output)->data;
  for (y=0; y<wheight; y++) {
    if (im_use->xoffset == 0 &&
	im_use->format == ZPixmap &&
	im_use->byte_order == LSBFirst &&
	im_use->depth == 24 &&
	im_use->bits_per_pixel == 32 &&
	im_use->red_mask   == 0x0000ffU &&
	im_use->green_mask == 0x00ff00U &&
	im_use->blue_mask  == 0xff0000U) {
      const char *p= im_use->data + y * im_use->bytes_per_line;
//      debugf("optimised copy y=%d",y);
      memcpy(op32, p, wwidth*sizeof(*op32));
      op32 += wwidth;
    } else {
      for (x=0; x<wwidth; x++) {
	long xrgb= XGetPixel(im_use,x,y);
	Rgb sample= 0;
	for (i=0; i<3; i++) {
	  sample <<= 8;
	  sample |=
	    ((xrgb << shiftmasks[i].lshift) >> shiftmasks[i].rshift)
	    & SAMPLEMASK;
	}
	*op32++= sample;
      }
    }
  }

  rtimestamp(&begin,"w*h*XGetPixel");
  if (im_free)
    XDestroyImage(im_free);
  
  rtimestamp(&begin,"XDestroyImage");
  check_not_disturbed();

  debugf("PAGING   snapshot done.\n");
}

static int identical(const Snapshot *a, const Snapshot *b) {
  if (!(a->w == b->w &&
	a->h == b->h))
    return 0;

  int compare_to= a->h;
  if (max_relevant_y>=0 && compare_to > max_relevant_y)
    compare_to= max_relevant_y;
  
  return !memcmp(a->data, b->data, a->w * 3 * compare_to);
}

static void wait_for_stability(Snapshot **output,
			       const Snapshot *previously,
			       void (*with_keypress)(void),
			       const char *fmt, ...)
     FMT(4,5);

static void wait_for_stability(Snapshot **output,
			       const Snapshot *previously,
			       void (*with_keypress)(void),
			       const char *fmt, ...) {
  va_list al;
  va_start(al,fmt);

  Snapshot *last=0;
  int nidentical=0;
  /* waits longer if we're going to return an image identical to previously
   * if previously==0, all images are considered identical to it */

  char *doing;
  sysassert( vasprintf(&doing,fmt,al) >=0 );

  debugf("PAGING  wait_for_stability"
	  "  last_input=%f previously=%p `%s'\n",
	  last_input, previously, doing);

  double max_interval= 1.000;
  double min_interval= 0.100;
  for (;;) {
    progress_spinner("%s",doing);
    
    double since_last_input= timestamp() - last_input;
    double this_interval= min_interval - since_last_input;
    if (this_interval > max_interval) this_interval= max_interval;

    if (this_interval >= 0)
      delay(this_interval);

    snapshot(output);

    if (!last) {
      debugf("PAGING  wait_for_stability first...\n");
      last=*output; *output=0;
    } else if (!identical(*output,last)) {
      debugf("PAGING  wait_for_stability changed...\n");
      free_snapshot(&last); last=*output; *output=0;
      nidentical=0;
      if (!with_keypress) {
	min_interval *= 3.0;
	min_interval += 0.1;
      }
    } else {
      nidentical++;
      int threshold=
	!previously ? 3 :
	identical(*output,previously) ? 5
	: 1;
      debugf("PAGING  wait_for_stability nidentical=%d threshold=%d\n",
	      nidentical, threshold);
      if (nidentical >= threshold)
	break;

      min_interval += 0.1;
      min_interval *= 2.0;
    }

    if (with_keypress)
      with_keypress();
  }

  free_snapshot(&last);
  free(doing);
  debugf("PAGING  wait_for_stability done.\n");
  va_end(al);
}

static void raise_and_get_details(void) {
  int evbase,errbase,majver,minver;
  int wxpos, wypos;
  unsigned bd,depth;

  progress("raising and checking YPP client window...");

  debugf("PAGING raise_and_get_details\n");

  int xtest= XTestQueryExtension(disp, &evbase,&errbase,&majver,&minver);
  if (!xtest) fatal("X server does not support the XTEST extension.");

  xassert( XRaiseWindow(disp, id) );
  /* in case VisibilityNotify triggers right away before we have had a
   * change to raise; to avoid falsely detecting lowering in that case */
  
  xassert( XSelectInput(disp, id,
			StructureNotifyMask|
			VisibilityChangeMask
			) );

  xassert( XRaiseWindow(disp, id) );
  /* in case the window was lowered between our Raise and our SelectInput;
   * to avoid failing to detect that lowering */

  xassert( XGetWindowAttributes(disp, id, &attr) );
  xassert( XGetGeometry(disp,id, &attr.root,
			&wxpos,&wypos, &wwidth,&wheight,
			&bd,&depth) );

  if (!(wwidth >= 320 && wheight >= 320))
    fatal("YPP client window is implausibly small?");

  if (attr.depth < 24)
    fatal("Display is not 24bpp.");

  check_client_window_all_on_screen();

  Bool shmpixmaps=0;
  int major=0,minor=0;
  int shm= XShmQueryVersion(disp, &major,&minor,&shmpixmaps);
  debugf("PAGING shm=%d %d.%d pixmaps=%d\n",shm,major,minor,shmpixmaps);
  if (shm) {
    xassert( shmim= XShmCreateImage(disp, attr.visual, attr.depth, ZPixmap,
				    0,&shminfo, wwidth,wheight) );

    sigset_t oldset, all;
    sigfillset(&all);
    sysassert(! sigprocmask(SIG_BLOCK,&all,&oldset) );

    int pfd[2];
    pid_t cleaner;
    sysassert(! pipe(pfd) );
    sysassert( (cleaner= fork()) != -1 );
    if (!cleaner) {
      sysassert(! close(pfd[1]) );
      for (;;) {
	int r= read(pfd[0], &shminfo.shmid, sizeof(shminfo.shmid));
	if (!r) exit(0);
	if (r==sizeof(shminfo.shmid)) break;
	assert(r==-1 && errno==EINTR);
      }
      for (;;) {
	char bc;
	int r= read(pfd[0],&bc,1);
	if (r>=0) break;
	assert(r==-1 && errno==EINTR);
      }
      sysassert(! shmctl(shminfo.shmid,IPC_RMID,0) );
      exit(0);
    }
    sysassert(! close(pfd[0]) );

    sysassert(! sigprocmask(SIG_SETMASK,&oldset,0) );

    assert(shmim->height == wheight);
    sysassert( (shminfo.shmid=
		shmget(IPC_PRIVATE, shmim->bytes_per_line * wheight,
		       IPC_CREAT|0600)) >= 0 );

    sysassert( write(pfd[1],&shminfo.shmid,sizeof(shminfo.shmid)) ==
	       sizeof(shminfo.shmid) );
    sysassert( shminfo.shmaddr= shmat(shminfo.shmid,0,0) );
    shmim->data= shminfo.shmaddr;
    shminfo.readOnly= False;
    xassert( XShmAttach(disp,&shminfo) );

    close(pfd[1]); /* causes IPC_RMID */
  }
}

static void set_focus_commodity(void) {
  int screen= XScreenNumberOfScreen(attr.screen);

  progress("taking control of YPP client window...");

  debugf("PAGING set_focus\n");

  send_mouse_1_updown(commod_focus_point.x, commod_focus_point.y);
  sync_after_input();

  delay(0.5);
  xassert( XSelectInput(disp, id,
			StructureNotifyMask|
			VisibilityChangeMask|
			FocusChangeMask
			) );

  int xpos,ypos;
  translate_coords_toroot_p(commod_page_point, &xpos,&ypos);
  XTestFakeMotionEvent(disp, screen, xpos,ypos, 0);

  sync_after_input();

  debugf("PAGING raise_and_set_focus done.\n");
}

static CanonImage *convert_page(const Snapshot *sn, RgbImage **rgb_r) {
  CanonImage *im;
  RgbImage *ri;

  fwrite_ppmraw(screenshot_file, sn);

  const Rgb *pixel= sn->data;
  CANONICALISE_IMAGE(im, sn->w, sn->h, ri, {
    rgb= *pixel++;
  });

  sysassert(!ferror(screenshot_file));
  sysassert(!fflush(screenshot_file));

  if (rgb_r) *rgb_r= ri;
  else free(ri);

  return im;
}

static void prepare_ypp_client(void) {
  CanonImage *test;
  Snapshot *current=0;
  
  /* find the window and check it's on the right kind of screen */
  raise_and_get_details();
  wait_for_stability(&current,0,0, "checking current YPP client screen...");

  test= convert_page(current,0);
  find_structure(test,0, &max_relevant_y,
		 &commod_focus_point,
		 &commod_page_point,
		 &commod_focuslast_point);
  check_correct_commodities();
  Rect sunshine= find_sunshine_widget();

  progress("poking client...");
  send_mouse_1_updown((sunshine.tl.x   + sunshine.br.x) / 2,
		      (sunshine.tl.y*9 + sunshine.br.y) / 10);
  sync_after_input();

  free(test);

  wait_for_stability(&current,0,0, "checking basic YPP client screen...");
  send_mouse_1_updown(250, wheight-10);
  send_mouse_1_updown_here();
  send_mouse_1_updown_here();
  sync_after_input();
  check_not_disturbed();
  SEND_KEY(XK_slash);
  SEND_KEY(XK_w);
  SEND_KEY(XK_Return);
  sync_after_input();

  Snapshot *status=0;
  wait_for_stability(&status,current,0, "awaiting status information...");
  free_snapshot(&current);
  free_snapshot(&status);
}

static void convert_store_page(Snapshot *current) {
  RgbImage *rgb;
  CanonImage *ci;
  PageStruct *pstruct;
  
  progress("page %d prescanning   ...",npages);
  ci= convert_page(current,&rgb);

  progress("page %d overview      ...",npages);
  find_structure(ci,&pstruct, 0,0,0,0);

  store_current_page(ci,pstruct,rgb);
}

void take_screenshots(void) {
  Snapshot *current=0, *last=0;

  prepare_ypp_client();
  
  /* page to the top - keep pressing page up until the image stops changing */
  set_focus_commodity();
  wait_for_stability(&current,0, send_pgup_many,
		     "paging up to top of commodity list...");

  /* now to actually page down */
  for (;;) {
    debugf("page %d paging\n",npages);

    pgdown_by_mouse();

    if (!(npages < MAX_PAGES))
      fatal("Paging down seems to generate too many pages - max is %d.",
	    MAX_PAGES);

    convert_store_page(current);
    free_snapshot(&last); last=current; current=0;
    debugf("PAGING page %d converted\n",npages);
    npages++;

    wait_for_stability(&current,last, 0,
		       "page %d collecting    ...",
		       npages);
    if (identical(current,last)) {
      free_snapshot(&current);
      break;
    }
  }
  progress("finishing with the YPP client...");
  send_mouse_1_updown(commod_focuslast_point.x, commod_focuslast_point.y);
  sync_after_input();
  send_pgdown_torestore();
  sync_after_input();

  debugf("PAGING all done.\n");
  progress_log("collected %d screenshots.",npages);
  check_pager_motion(0,npages);
}    

void take_one_screenshot(void) {
  Snapshot *current=0;

  prepare_ypp_client();
  wait_for_stability(&current,0,0, "taking screenshot...");
  convert_store_page(current);
  npages= 1;
  progress_log("collected single screenshot.");
}

void set_yppclient_window(unsigned long wul) {
  id= wul;
}

DEBUG_DEFINE_SOME_DEBUGF(findypp,debugfind)

static int nfound;
static Atom wm_name;
static int screen;

static void findypp_recurse(int depth, int targetdepth, Window w) {
  unsigned int nchildren;
  int i;
  Window *children=0;
  Window gotroot, gotparent;

  static const char prefix[]= "Puzzle Pirates - ";
  static const char onthe[]= " on the ";
  static const char suffix[]= " ocean";
#define S(x) ((int)sizeof((x))-1)

  debugfind("FINDYPP %d/%d screen %d  %*s %lx",
	    depth,targetdepth,screen,
	    depth,"",(unsigned long)w);
  
  if (depth!=targetdepth) {
    xassert( XQueryTree(disp,w,
			&gotroot,&gotparent,
			&children,&nchildren) );
    debugfind(" nchildren=%d\n",nchildren);
  
    for (i=0; i<nchildren; i++) {
      Window child= children[i];
      findypp_recurse(depth+1, targetdepth, child);
    }
    XFree(children);
    return;
  }
    

  int gotfmt;
  Atom gottype;
  unsigned long len, gotbytesafter;
  char *title;
  unsigned char *gottitle=0;
  xassert( !XGetWindowProperty(disp,w, wm_name,0,512, False,
			       AnyPropertyType,&gottype, &gotfmt, &len,
			       &gotbytesafter, &gottitle) );
  title= (char*)gottitle;

  if (DEBUGP(findypp)) {
    debugfind(" gf=%d len=%lu gba=%lu \"", gotfmt,len,gotbytesafter);
    char *p;
    for (p=title; p < title+len; p++) {
      char c= *p;
      if (c>=' ' && c<=126) fputc(c,debug);
      else fprintf(debug,"\\x%02x",c & 0xff);
    }
    fputs("\": ",debug);
  }

#define REQUIRE(pred)							\
  if (!(pred)) { debugfind(" failed test  %s\n", #pred); return; }	\
  else

  REQUIRE( gottype!=None );
  REQUIRE( len );
  REQUIRE( gotfmt==8 );

  REQUIRE( len >= S(prefix) + 1 + S(onthe) + 1 + S(suffix) );

  char *spc1= strchr(  title        + S(prefix), ' ');  REQUIRE(spc1);
  char *spc2= strrchr((title + len) - S(suffix), ' ');  REQUIRE(spc2);

  REQUIRE( (title + len) - spc1  >= S(onthe)  + S(suffix) );
  REQUIRE(  spc2         - title >= S(prefix) + S(onthe) );

  REQUIRE( !memcmp(title,                   prefix, S(prefix)) );
  REQUIRE( !memcmp(title + len - S(suffix), suffix, S(suffix))  );
  REQUIRE( !memcmp(spc1,                    onthe,  S(onthe))  );

#define ASSIGN(what, start, end)			\
  what= masprintf("%.*s", (int)((end)-(start)), start);	\
  if (o_##what) REQUIRE( !strcasecmp(o_##what, what) );	\
  else

  ASSIGN(ocean,  spc1 + S(onthe),   (title + len) - S(suffix));
  ASSIGN(pirate, title + S(prefix),  spc1);

  debugfind(" YES!\n");
  id= w;
  nfound++;
  progress_log("found YPP client (0x%lx):"
	       " %s ocean - %s.",
	       (unsigned long)id, ocean, pirate);
}

void find_yppclient_window(void) {
  int targetdepth;

  nfound=0;
  
  if (id) return;
  
  progress("looking for YPP client window...");

  xassert( (wm_name= XInternAtom(disp,"WM_NAME",True)) != None);

  for (targetdepth=1; targetdepth<4; targetdepth++) {
    for (screen=0; screen<ScreenCount(disp); screen++) {
      debugfind("FINDYPP screen %d\n", screen);
      findypp_recurse(0,targetdepth, RootWindow(disp,screen));
    }
    if (nfound) break;
  }

  if (nfound>1)
    fatal("Found several possible YPP clients.   Close one,\n"
	  " disambiguate with --pirate or --ocean,"
	  " or specify --window-id.\n");
  if (nfound<1)
    fatal("Did not find %sYPP client."
	  " Use --window-id and/or report this as a fault.\n",
	  o_ocean || o_pirate ? "matching ": "");
}

void screenshot_startup(void) {
  progress("starting...");
  disp= XOpenDisplay(0);
  if (!disp) fatal("Unable to open X11 display.");
  sysassert(! gettimeofday(&tv_startup,0) );

  keymap_startup();
}
