# This is part of ypp-sc-tools, a set of third-party tools for assisting
# players of Yohoho Puzzle Pirates.
#
# Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
# are used without permission.  This program is not endorsed or
# sponsored by Three Rings.

package CommodsDatabase;

# Valid calling sequences:
#    db_setocean('Midnight')
#  [ db_filename() => 'OCEAN-Midnight.db'  also OK at any later time ]
#  [ db_writer() ]                         helpful but not essential
#    db_connect()
#  [ db_onconflict(sub { .... }) ]         essential if just dieing is not OK
#    $dbh->do(...), $dbh->prepare(...), db_doall("stmt;stmt;"), etc.

use strict;
use warnings;

use DBI;
use POSIX;
use DBD::SQLite;

use Commods;

BEGIN {
    use Exporter ();
    our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
    $VERSION     = 1.00;
    @ISA         = qw(Exporter);
    @EXPORT      = qw(&db_setocean &db_writer &db_connect $dbh
		      &db_filename &db_doall &db_onconflict
		      &dbr_filename &dbr_connect &db_connect_core
		      &dumptab_head &dumptab_row_hashref
		      &db_chkcommit &db_check_referential_integrity);
    %EXPORT_TAGS = ( );

    @EXPORT_OK   = qw();
}

sub dbr_filename ($$) {
    my ($datadir,$oceanname) = @_;
    return "$datadir/OCEAN-$oceanname.db";
}
sub dbr_connect ($$) {
    my ($datadir,$ocean) = @_;
    return db_connect_core(dbr_filename($datadir,$ocean));
}

sub db_connect_core ($) {
    my ($fn)= @_;
    my $opts = { AutoCommit=>0,
		 RaiseError=>1, ShowErrorStatement=>1,
		 sqlite_unicode=>1 };

    # DBI now wants to start a transaction whenever we even say
    # SELECT.  But this doesn't work if the DB is readonly.  We can
    # work around this by setting autocommit, in which case there is
    # no need for a transaction for read-only db commands.  Autocommit
    # is (obviously) safe with readonly operations.  But callers in
    # yarrg do not specify to us whether they intend to write.  So we
    # decide, by looking at the file mode.  And as belt-and-braces we
    # set sqlite's own readonly flag as well.
    # http://stackoverflow.com/questions/30082008/attempt-to-write-a-readonly-database-but-im-not
    # http://stackoverflow.com/questions/35208727/can-sqlite-db-files-be-made-read-only
    # http://cpansearch.perl.org/src/ISHIGAKI/DBD-SQLite-1.39/Changes
    # (see entry for 1.38_01)
    # http://stackoverflow.com/questions/17793672/perl-dbi-treats-setting-sqlite-db-cache-size-as-a-write-operation-when-subclassi
    # https://rt.cpan.org/Public/Bug/Display.html?id=56444#
    my $readonly =
	(access $fn, POSIX::W_OK) ? 0 :
	($! == EACCES) ? 1 :
	($! == ENOENT) ? 0 :
	die "$fn access(,W_OK) $!";
    if ($readonly) {
	$opts->{sqlite_open_flags} = DBD::SQLite::OPEN_READONLY;
	$opts->{AutoCommit}=1;
    }

    my $h= DBI->connect("dbi:SQLite:$fn",'','',$opts)
	or die "$fn $DBI::errstr ?";
    return $h;
    # default timeout is 30s which is plenty
}

our $dbfn;
our $dbh;

sub db_setocean ($) {
    my ($oceanname) = @_;
    $dbfn= dbr_filename('.',$oceanname);
}
sub db_filename () {
    return $dbfn;
}

sub db_onconflict (&) {
    my ($conflictproc) = @_;
    $dbh->{HandleError}= sub {
	my ($emsg,$dbh,$val1) = @_;
	my $native_ecode= $dbh->err();
	&$conflictproc($emsg) if grep { $_ == $native_ecode } qw(5 6);
	# 5==SQLITE_BUSY, 6==SQLITE_LOCKED according to the SQLite3
	# API documentation, .../capi3ref.html#extended-result-codes.
	return 0; # RaiseError happens next.
    };
}

our $writerlockh;

sub db_writer () {
    my $lockfn= "Writer.lock";
    $writerlockh= new IO::File "$lockfn", "w" or die "$lockfn $!";

    my $flockall= pack 's!s!LLLLLL', F_WRLCK, SEEK_SET, 0,0,0,0,0,0;
    # should work everywhere to lock the whole file, provided that
    # l_type and l_whence are `short int' and come first in that order,
    # and that start, len and pid are no more than 64 bits each.

    my $r= fcntl($writerlockh, F_SETLKW, $flockall);
    $r or die "$lockfn fcntl $!";
}

sub db_connect () {
    $dbh= db_connect_core($dbfn);
}

sub db_doall ($) {
    foreach my $cmd (split /\;/, $_[0]) {
	$dbh->do("$cmd;") if $cmd =~ m/\S/;
    }
}

#---------- table dump helper ----------

sub dumptab_head ($$$) {
    my ($fh,$w,$cols) = @_;
    printf $fh "|%-${w}s", $_ foreach @$cols;  print $fh "|\n";
    print $fh "+",('-'x$w)  foreach @$cols;    print $fh "+\n";
}

sub dumptab_row_hashref ($$$$) {
    my ($fh,$w,$cols,$row) = @_;
    printf $fh "|%-$w.${w}s",
	   (defined $row->{$_} ? $row->{$_} : 'NULL')
	foreach @$cols;
    print $fh "\n";
}

#---------- referential integrity constraints ----------

# SQLite doesn't support foreign key constraints so we do it by steam:

sub nooutput ($) {
    my ($stmts) = @_;
    my $ekindcount= 0;
    my $letxt= '';
    foreach my $stmt (split /\;/, $stmts) {
	next unless $stmt =~ /\S/;

	my $etxt= '';
	$stmt =~ s/^([ \t]*\#.*)$/ $etxt .= $1."\n"; ''; /mge;
	$etxt= $letxt unless length $etxt;
	$letxt= $etxt;
	
	$stmt =~ s/^\s+//; $stmt =~ s/\s+$//;
	my $sth= $dbh->prepare($stmt);
	$sth->execute();
	my $row;
	my $ecount= 0;
	my @cols= @{ $sth->{NAME_lc} };
	my $w= 11;
	while ($row= $sth->fetchrow_hashref) {
	    if (!$ecount++) {
		print STDERR "REFERENTIAL INTEGRITY ERROR\n";
		print STDERR "\n$etxt\n $stmt\n\n";
		dumptab_head(\*STDERR,$w,\@cols);
	    }
	    if ($ecount>5) { print STDERR "...\n"; last; }
	    dumptab_row_hashref(\*STDERR,$w,\@cols,$row);
	}
	next unless $ecount;
	
	$ekindcount++;
	print STDERR "\n\n";
    }
    die "REFERENTIAL INTEGRITY ERRORS $ekindcount\n"
	if $ekindcount;
}

sub db_check_referential_integrity ($) {
    my ($full) = @_;
    # non-full is done only for market data updates; it avoids
    # detecting errors which are essentially missing metadata and
    # old schemas, etc.

    foreach my $bs (qw(buy sell)) {
	nooutput(<<END);

 # Every buy/sell must refer to an entry in commods, islands, and stalls:
 SELECT * FROM $bs LEFT JOIN commods USING (commodid) WHERE commodname IS NULL;
 SELECT * FROM $bs LEFT JOIN islands USING (islandid) WHERE islandname IS NULL;
 SELECT * FROM $bs LEFT JOIN stalls USING (stallid, islandid)
                                                      WHERE stallname IS NULL;

 # Every buy/sell must be part of an upload:
 SELECT * FROM $bs LEFT JOIN uploads USING (islandid) WHERE timestamp IS NULL;

 # The islandid in stalls must be the same as the islandid in buy/sell:
 SELECT * FROM $bs JOIN stalls USING (stallid)
	WHERE $bs.islandid != stalls.islandid;

END
    }

    nooutput(<<END);

 # Every stall and upload must refer to an island:
 SELECT * FROM stalls LEFT JOIN islands USING (islandid)
                                        WHERE islandname IS NULL;
 SELECT * FROM uploads LEFT JOIN islands USING (islandid)
                                         WHERE islandname IS NULL;

END
    if ($full) {
	foreach my $end (qw(aiid biid)) {
	    foreach my $tab (qw(dists routes)) {
		nooutput(<<END);

 # Every row in dists and routes must refer to two existing rows in islands:
 SELECT * FROM $tab d LEFT JOIN islands ON d.$end=islandid
	WHERE islandname IS NULL;

END
	    }
	}
	nooutput(<<END);

 # Every pair of islands must have an entry in dists:
 SELECT * FROM islands ia JOIN islands ib LEFT JOIN dists
	ON ia.islandid=aiid and ib.islandid=biid
	WHERE dist IS NULL;

 # Every commod must refers to a commodclass and vice versa:
 SELECT * FROM commods LEFT JOIN commodclasses USING (commodclassid)
	WHERE commodclass IS NULL;
 SELECT * FROM commodclasses LEFT JOIN commods USING (commodclassid)
	WHERE commodname IS NULL;

 # Ordvals which are not commodclass ordvals are unique:
 SELECT ordval,count(*),commodname,commodid,posinclass
	FROM commods
	WHERE posinclass > 0
	GROUP BY ordval
	HAVING count(*) > 1;

 # For every class, posinclass is dense from 1 to maxposinclass,
 # apart from the commods for which it is zero.
 SELECT commodclass,commodclassid,posinclass,count(*)
	FROM commods JOIN commodclasses USING (commodclassid)
	WHERE posinclass > 0
	GROUP BY commodclassid,posinclass
	HAVING count(*) > 1;
 SELECT commodclass,commodclassid,count(*)
	FROM commods JOIN commodclasses USING (commodclassid)
	WHERE posinclass > 0
	GROUP BY commodclassid
	HAVING count(*) != maxposinclass;
 SELECT *
	FROM commods JOIN commodclasses USING (commodclassid)
	WHERE posinclass < 0 OR posinclass > maxposinclass;

END
    }
}

sub db_chkcommit ($) {
    my ($full) = @_;
    db_check_referential_integrity($full);
    $dbh->commit();
}

1;
