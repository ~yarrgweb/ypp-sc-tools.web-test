/*
 * Route searcher - command line parsing and result printing
 */
/*
 *  This is part of the YARRG website, a tool for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#include "rscommon.h"

#include <ctype.h>

int o_quiet= 0;
double max_mass=-1, max_volu=-1, max_capi=-1;
double distance_loss_factor_per_league;
int max_dist=-1, min_trade_maxprofit=0;

FILE *debug_file;
FILE *output;

#define tabdebugf printf


#define CTR(x)    int ctr_##x;
#define CTRA(x,n) int ctr_##x[n];
  COUNTER_LIST
#undef CTR
#undef CTRA

static Bucket ****results[GRANUS];
  /* results[GRANUS][start_isle_ix][finalisle][midisle]-> */

static pid_t debugoutpid;

int main(int argc, const char **argv) {
  const char *arg;
  int i, ap;
  int granui;
  const char *database=0;
  const char *concur_base=0, *concur_rhs=0;
  int concur_lim=-1;

#ifndef debug_flags
  debug_flags= ~( dbg_sql2 );
#endif

  for (;;) {
    arg= *++argv;
    if (arg[0] != '-') break;
    if (!strcmp(arg,"-d")) {
      database= *++argv;
    } else if (!strcmp(arg,"-C")) {
      concur_base= *++argv;
      concur_rhs= *++argv;
      concur_lim= atoi(*++argv);
    } else if (!strcmp(arg,"-g")) {
      granus= atoi(*++argv);
      assert(granus>=1 && granus<=GRANUS);
#ifndef debug_flags
    } else if (!strcmp(arg,"-DN")) {
      debug_flags= 0;
    } else if (!strcmp(arg,"-D1")) {
      debug_flags= ~(dbg_sql2|dbg_lp|dbg_value2);
#endif
    } else {
      abort();
    }
  }

  if (debug_flags) {
    /* glpk insists on writing stuff to stdout, and it does buffering,
     * so we route all our debug through it this too */
    int realstdout;
    sysassert( (realstdout= dup(1)) > 2 );
    sysassert( output= fdopen(realstdout,"w") );

    int pfd[2];
    sysassert(! pipe(pfd) );
    sysassert( (debugoutpid= fork()) >=0 );
    if (!debugoutpid) {
      sysassert( dup2(pfd[0],0)==0 );
      sysassert( dup2(2,1)==1 );
      sysassert(! close(pfd[0]) );
      sysassert(! close(pfd[1]) );
      sysassert(! execlp("cat","cat",(char*)0) );
    }
    sysassert( dup2(pfd[1],1)==1 );
    sysassert(! close(pfd[0]) );
    sysassert(! close(pfd[1]) );

    debug_file= stdout;
  } else {
    output= stdout;
    debug_file= stderr;
  }

  sysassert( !setvbuf(debug,0,_IOLBF,0) );

  max_mass= atof(*argv++);
  max_volu= atof(*argv++);
  max_capi= atof(*argv++);
  double loss_per_league= atof(*argv++);
  distance_loss_factor_per_league= 1.0 - loss_per_league;

  min_trade_maxprofit= atoi(*argv++);

  if (concur_base) {
    for (i=0; i<concur_lim; i++) {
      char *concfn= masprintf("%s%02d%s", concur_base, i, concur_rhs);
      int concfd, r;
      sysassert( (concfd= open(concfn, O_RDWR|O_CREAT|O_TRUNC, 0600)) >= 0);
      struct flock fl;
      memset(&fl,0,sizeof(fl));
      fl.l_type= F_WRLCK;
      r= fcntl(concfd, F_SETLK, &fl);
      free(concfn);
      if (!r) goto concur_ok;
      sysassert( errno == EWOULDBLOCK );
      close(concfd);
    }
    fprintf(output,"@@@ concurrency limit exceeded (%d)\n", concur_lim);
    exit(0);

  concur_ok:
    /* deliberately leak concfd */
    fprintf(output,"concurrency slot %d\n", i);
  }

  setup_sql(database);
  setup_value();
  setup_search();

  for (i=0; i<narches; i++)
    fprintf(output,"arch %d %s\n",i,archnames[i]);
  fprintf(output,"setup complete, starting search\n");

  arg= *argv++;
  if (!strcmp(arg,"specific")) {
    int ia[argc], ni=0;
    while ((arg= *argv++))
      ia[ni++]= atoi(arg);

    double val= value_route(ni, ia, 0);
    fprintf(output, "route value is %g\n", val);
  } else if (!strcmp(arg,"search")) {
    for (granui=0; granui<GRANUS; granui++)
      MCALLOC(results[granui], argc);

    max_dist= atoi(*argv++);

    for (ap=0; ap<AP; ap++) {
      int nhs= atoi(*argv++);
      for (granui=0; granui<GRANUS; granui++) {
	nhighscores[granui][ap]= nhs;
	MCALLOC(highscores[granui][ap], nhs);
      }
    }
    const char *final_isle_spec= *argv++;

    int resultsix= 0;
    while ((arg= argv[resultsix])) {
      int init_isle= atoi(arg);

      int final_isle;
      if (!strcmp(final_isle_spec,"circ")) final_isle= init_isle;
      else if (!strcmp(final_isle_spec,"any")) final_isle= -1;
      else final_isle= atoi(final_isle_spec);
      assert(final_isle);

      Bucket ****buckets_base_io[GRANUS];
      for (granui=0; granui<GRANUS; granui++)
	buckets_base_io[granui]= &results[granui][resultsix];
	
      search(init_isle, final_isle, buckets_base_io);
      resultsix++;
    }

    int mid, fin;
    for (granui=0; granui<granus; granui++) {
      fprintf(output,"\n");
      for (i=0; i<resultsix; i++) {
	tabdebugf("========== start #%d granui%d %s ==========\n",
		  i, granui, argv[i]);
	Bucket ***buckets_resultsix= results[granui][i];
	if (!buckets_resultsix) continue;
	tabdebugf("    ");
	for (mid=0; mid<granusz_mid[granui]; mid++) {
	  tabdebugf("|   m%-3d   ",mid);
	}
	tabdebugf("\n");
	for (fin=0; fin<granusz_fin[granui]; fin++) {
	  Bucket **buckets_fin= buckets_resultsix[fin];
	  if (!buckets_fin) continue;
	  tabdebugf("f%-3d",fin);
	  for (mid=0; mid<granusz_mid[granui]; mid++) {
	    Bucket *result= buckets_fin[mid];
	    if (!result) {
	      tabdebugf("|          ");
	    } else {
	      tabdebugf("|%5d",(int)(result->prs[A].value[A]));
	      tabdebugf(" ");
	      tabdebugf("%4d",(int)(result->prs[P].value[P]));
	    }
	  }
	  tabdebugf("\n");
	}
      } /* i */

      for (ap=0; ap<AP; ap++) {
	int pos;
	fprintf(output,"============== granui%d ap=%d ==============\n",
		granui, ap);
	for (pos=nhighscores[granui][ap]-1; pos>=0; pos--) {
	  HighScoreEntry *hs= &highscores[granui][ap][pos];
	  Bucket *bucket= hs->bucket;
	  if (!bucket) continue;
	  OnePotentialResult *pr= &bucket->prs[ap];
	  const int *const ports= pr->ports;
	  int nports;
	  for (nports=0; nports<MAX_ROUTELEN && ports[nports]>=0; nports++);
	  int finisle= ports[nports-1];
	  int finarch= isle2arch(finisle);
	  int midisle= ports[nports/2];
	  int midarch= route2midarch(ports,nports);
	  fprintf(output,
		  " @%2d %c#%2d | start%3d mid%d:%3d f%d:%3d"
		  " | %3dlg | %5d %5d %4d |",
		  pos, "ap"[ap], nhighscores[granui][ap] - pos,
		  ports[0], midarch,midisle, finarch,finisle, pr->length,
		  (int)hs->value, (int)pr->value[A], (int)pr->value[P]);
	  for (i=0; i<nports; i++) fprintf(output," %d",ports[i]);
	  fprintf(output,"\n");
	} /* pos */
      } /* ap */
    } /* granui */
    fprintf(output,"\n");

  } else {
    abort();
  }

#define CTR(x) fprintf(output,"  %-30s %10d\n",#x,ctr_##x);
#define CTRA(x,n) for (i=0;i<n;i++) \
  fprintf(output,"  %-27s[%d] %10d\n",#x,i,ctr_##x[i]);
  COUNTER_LIST
#undef CTR

  if (debug_flags) {
    sysassert(! fclose(debug) );
    waitpid_check_exitstatus(debugoutpid,"debug cat",1);
  }
  sysassert(! fclose(output) );

  return 0;
}
