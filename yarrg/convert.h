/*
 * general header file for yarrg
 */
/*
 *  This is part of ypp-sc-tools, a set of third-party tools for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#ifndef CONVERT_H
#define CONVERT_H

#define DEBUG_FLAG_LIST				\
   DF(findypp)					\
   DF(pages)					\
   DF(keymap)					\
   DF(rect)					\
   DF(pixmap)					\
   DF(struct)					\
   DF(ocr)					\
   DF(rsync)					\
   DF(structcolon)				\
   DF(callout)


#include "common.h"
#include "ocr.h"

#include <fnmatch.h>
#include <pcre.h>
#include <pam.h>
#include <time.h>
#include <limits.h>
#include <ctype.h>

#include <sys/time.h>


/*----- from rgbimage.c -----*/

#define MAXIMGIDENT 100

typedef uint32_t Rgb; /* red<<0 | green<<8 | blue<<16 */

typedef struct RgbImage {
  int w, h;
  Rgb data[]; /* data[ y*w + x ] */
} RgbImage;

void identify_rgbimage(const RgbImage *base, Rect portion,
		       char result[MAXIMGIDENT], const char *what);
RgbImage *alloc_rgb_image(int w, int h);
void fwrite_ppmraw(FILE *f, const RgbImage *ri);

#define RI_PIXEL32(ri,x,y) ((ri)->data + ((y)*(ri)->w + (x)))

static inline Rgb ri_rgb(const RgbImage *ri, int x, int y) {
  return *RI_PIXEL32(ri,x,y);
}

/*----- from structure.c -----*/

typedef struct PageStruct PageStruct;

void find_structure(const CanonImage *im,
		    PageStruct **pagestruct_r, /* caller must free() */
		    int *max_relevant_y_r,
		    Point *commod_focus_point_r,
		    Point *commod_page_point_r,
		    Point *commod_focuslast_point_r);
void store_current_page(CanonImage *ci /*pointer saved*/,
			PageStruct *pagestruct, /*freed*/
			RgbImage *rgb /*freed*/);
void adjust_colours(CanonImage *ci, const RgbImage *rgb);

void select_page(int page);
void check_pager_motion(int first, int stop);

Rect find_sunshine_widget(void);

void canon_colour_prepare(void);
void find_islandname(void);
void check_correct_commodities(void);
void read_screenshots(void);
void read_one_screenshot(void);
void analyse(FILE *tsv_output);

/*----- from convert.c -----*/

extern FILE *screenshot_file;
void fetch_with_rsync(const char *stem);
void fetch_with_rsync_gz(const char *stem);

enum flags {
  ff_singlepage=          000002,
  ff_testservers=         000004,

  ff_charset_edit=        000010,
  ff_charset_allowedit=   000020,
  ff_charset_havelocal=   000040,
  ffm_charset=            000070,
  
  ff_dict_fetch=          000100,
  ff_dict_submit=         000200,
  ff_dict_pirate=         000400,
  ffs_dict=               000700,

  ff_needisland=          001000,
  ff_printisland=         002000,
  ff_upload=              004000,
  
  ff_use_yarrg=           010000,
  ff_use_pctb=            020000,
  ffm_use=                070000,
  
  ffs_printisland=        003000,
  ffs_upload=             005000,
};
extern enum flags o_flags;

enum mode {
  mf_findwindow=        00001,
  mf_screenshot=        00010,
  mf_readscreenshot=    00020,
  mf_analyse=           00100,
  mf_printoceanpirate=  00200,
  mfm_special=          07000,
  
  mode_findwindow=      00201,
  mode_screenshot=      00011,
  mode_analyse=         00120,
  mode_showcharset=     01000,

  mode_all=           00111,
};
extern enum mode o_mode;

extern const char *o_ocean, *o_pirate;
extern int o_quiet;


#define dbassert(x) ((x) ? (void)0 : dbfile_assertfail(__FILE__,__LINE__,#x))
void dbfile_assertfail(const char *file, int line, const char *m) NORET;

FILE *dbfile;
void dbfile_getsline(char *lbuf, size_t lbufsz, const char *file, int line);
int dbfile_open(const char *tpath);   /* 0: ENOENT; 1: worked */
int dbfile_gzopen(const char *tpath); /* 0: ENOENT; 1: worked */
void dbfile_close(void); /* idempotent */

int dbfile_scanf(const char *fmt, ...) SCANFMT(1,2);
int dbfile_vscanf(const char *fmt, va_list al) SCANFMT(1,0);

int gzopen(const char *zpath, int oflags, FILE **f_r, pid_t *pid_r,
	   const char *gziplevel /* 0 for read; may be 0, or "-1" etc. */);
  /* returns errno value from open */
void gzclose(FILE **f, pid_t *p, const char *what);
  /* also OK with f==0, or p==-1 */


const char *get_vardir(void);
const char *get_libdir(void);


#define EXECLP_HELPER(helper, ...) do{				\
    char *helper_path= masprintf("%s/%s",get_libdir(),helper);	\
    execlp(helper_path,helper, __VA_ARGS__);			\
    sysassert(errno==ENOENT);					\
    fatal("Failed to find helper program %s.\n"			\
	  "(Are you in the correct directory?)", helper);	\
  }while(0)


/*----- from pages.c -----*/

void screenshot_startup(void);
void set_yppclient_window(unsigned long wul);
void find_yppclient_window(void);
void take_screenshots(void);
void take_one_screenshot(void);

#define MAX_PAGES 1000

extern const CanonImage *page_images[MAX_PAGES];
extern const RgbImage *page0_rgbimage;
extern int npages;

extern const char *ocean, *pirate;
extern char *archipelago, *island;

#endif /*CONVERT_H*/
