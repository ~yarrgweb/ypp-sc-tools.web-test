/*
 * Route searcher - common definitions
 */
/*
 *  This is part of the YARRG website, a tool for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#ifndef RSCOMMON_H
#define RSCOMMON_H

#include <sqlite3.h>

#define DEBUG_FLAG_LIST				\
   DF(sql)					\
   DF(sql2)					\
   DF(value)					\
   DF(value2)					\
   DF(search)					\
   DF(filter)					\
   DF(check)					\
   DF(tableau)					\
   DF(lp)

//#define debug_flags 0

#define debug debug_file

#include "common.h"

extern FILE *debug_file;
#define DEBUG_DEV "/dev/stdout" /* just for glpk */


#define GRANUS 3

#define COUNTER_LIST				\
   CTR(commodities_loaded)			\
   CTR(trades_loaded)				\
   CTR(islands_arbitrage)			\
   CTR(ipairs_relevant)				\
   CTR(quantities_loaded)			\
   CTR(routes_considered)			\
   CTR(routes_wrongfinalelim)			\
   CTR(routes_quickelim)			\
   CTR(routes_bucketelim)			\
   CTR(routes_valued)				\
   CTR(routes_wrongfinal)			\
   CTRA(newbests_granu,GRANUS*2)		\
   CTR(subroute_tails_valued)			\
   CTR(subroutes_valued)			\
   CTR(subroutes_nonempty)
#define CTR(x)    extern int ctr_##x;
#define CTRA(x,n) extern int ctr_##x[n];
  COUNTER_LIST
#undef CTR
#undef CTRA

#define SQL_MUST( call ) ({						 \
    /* `call' is an expression returning result, using  const char *sqe; \
     * chk1 and chk2 are blocks using sqe and  int sqr; */		 \
    const char *sql_must_call_string= #call;				 \
    int sqr;								 \
    if (DEBUGP(sql2)) fprintf(debug,"SQL %s", sql_must_call_string);	 \
    sqr= (call);							 \
    if (DEBUGP(sql2)) fprintf(debug," = %d\n", sqr);			 \
    if (sqr) sql_fatal("(unknown)", sqr, sql_must_call_string);		 \
  })									 \

void sql_fatal(const char *stmt_what, int sqr, const char *act_what) NORET;

#define SQL_STEP(ssh) (sql_step((ssh), #ssh, __FILE__, __LINE__))
int sql_step(sqlite3_stmt *ssh, const char *ssh_string,
	     const char *file, int line);

#define SQL_DISTINCT_DECL(cols, nintcols)	\
  int cols[nintcols];				\
  cols[0]= -1;
#define SQL_DISTINCT_STEP(ssh, cols, nkeycols)				 \
  (sql_step_distinct((ssh), #ssh, __FILE__, __LINE__,			 \
		     (cols), sizeof((cols))/sizeof((cols)[0]), nkeycols))
int sql_step_distinct(sqlite3_stmt *ssh, const char *ssh_string,
		      const char *file, int line,
		      int *cols, int ncols, int nkeycols);
   /* These work if we're making a query whose columns consist of:
    *  - keys: integer column(s) on which the results are sorted by the query
    *  - consequences: zero or more integer cols strictly dependent on the keys
    *  - extra: zero or more further (possibly non-integer) columns
    *
    * Call SQL_DISTINCT_DECL, passing intcols = the total number of keys and
    * consequences; it will declare  int cols[intfields];
    *
    * Then each SQL_DISTINCT_STEP is like SQL_STEP only you have to
    * pass the number of key columns and it only returns rows with
    * distinct keys.  Rows with all-identical keys are asserted to
    * have identical consequences.  After each call to
    * SQL_DISTINCT_STEP the keys and consequences will be stored in
    * cols.
    */

int sql_single_int(const char *stmt);

#define SQL_PREPARE(ss,stmt) ((ss)= sql_prepare((stmt),#ss))
sqlite3_stmt *sql_prepare(const char *stmt, const char *what);

#define SQL_BIND(ss,index,value) (sql_bind((ss),(index),(value),#ss,#value))
void sql_bind(sqlite3_stmt *ss, int index, int value,
	      const char *ss_what, const char *val_what);

#define MAX_ROUTELEN 20

extern sqlite3 *db;

void setup_sql(const char *database);


typedef struct {
  double distance_loss_factor;
  struct TradesBlock *trades;
  double route_tail_value;
} IslandPair;

IslandPair *ipair_get_maybe(int si, int di);

double value_route(int nislands, const int *islands, int exclude_arbitrage);
void setup_value(void);

#define AP 2 /* 0=absolute, 1=perleague */
#define A 0
#define P 1

typedef struct {
  double value[AP];
  int length;
  int ports[MAX_ROUTELEN];
} OnePotentialResult;

typedef struct {
  OnePotentialResult prs[AP];
} Bucket;

void setup_search(void);
void search(int start_isle, int final_isle /* -1 means any */,
	    Bucket ****buckets_base_io[GRANUS]
	        /* bucket_base[granui][finalthing][midthing]-> */);

extern double max_mass, max_volu, max_capi;
extern double distance_loss_factor_per_league;
extern int max_dist, min_trade_maxprofit;

#define LOSS_FACTOR_PER_DELAY_SLOT (1-1e-8)

extern int islandtablesz;

extern int narches;
extern char **archnames;
extern int *islandid2arch;

extern int granusz_fin[GRANUS], granusz_mid[GRANUS];


extern FILE *output;


#define NEW(ptr) ((ptr)= mmalloc(sizeof(*ptr)))

#define MCALLOC(array, count) ((array)= mcalloc(sizeof(*(array)) * (count)))

#define MCALLOC_INITEACH(array, count, init_this) ({                   \
    MCALLOC((array), (count));                                         \
    int initi;                                                         \
    typeof(&(array)[0]) this;                                          \
    for (initi=0, this=(array); initi<(count); initi++, this++) {      \
      init_this;                                                       \
    }                                                                  \
  })


typedef struct {
  double value;
  Bucket *bucket;
} HighScoreEntry;

extern int granus;
extern int nhighscores[GRANUS][AP];
extern HighScoreEntry *highscores[GRANUS][AP];


#define ONDEMAND(pointer_lvalue, calloc_size_count)			     \
  ((pointer_lvalue) ? :							     \
   ((pointer_lvalue) = mcalloc(sizeof(*(pointer_lvalue)) * calloc_size_count)))


static inline int isle2arch(int isle) {
  int arch= islandid2arch[isle];
  assert(arch>=0);
  return arch;
}

static inline int route2midarch(const int *ports, int nports) {
  int archs[nports], last_arch=-1, narchs=0, i;
  for (i=0; i<nports; i++) {
    int arch= isle2arch(ports[i]);
    if (arch==last_arch) continue;
    archs[narchs++]= last_arch= arch;
  }
  return archs[narchs/2];
}


#endif /*RSCOMMON_H*/
