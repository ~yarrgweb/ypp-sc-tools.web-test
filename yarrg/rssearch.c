/*
 * Route searcher - recursive iteration over all routes
 */
/*
 *  This is part of the YARRG website, a tool for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#include "rscommon.h"

DEBUG_DEFINE_DEBUGF(search);
DEBUG_DEFINE_SOME_DEBUGF(filter,fildebugf);

typedef struct Neighbour {
  struct Neighbour *next;
  int islandid;
  int dist;
} Neighbour;

static Neighbour **neighbours; /* neighbours[islandid]->islandid etc. */
static sqlite3_stmt *ss_neigh;

static int ports[MAX_ROUTELEN];
static int final_isle;

static Neighbour *get_neighbours(int isle) {
  Neighbour **np= &neighbours[isle];
  Neighbour *head= *np;
  if (head) return head;

  SQL_BIND(ss_neigh, 1, isle);
  while (SQL_STEP(ss_neigh)) {
    Neighbour *add;
    NEW(add);
    add->islandid= sqlite3_column_int(ss_neigh, 0);
    add->dist= sqlite3_column_int(ss_neigh, 1);
    add->next= head;
    head= add;
  }
  SQL_MUST( sqlite3_reset(ss_neigh) );

  *np= head;
  return head;
}


static Bucket ***buckets_base[GRANUS];


static double process_route(int nports, int totaldist,
			    double overestimate_excepting_tail) {
  int i, ap, granui;
  int leagues_divisor= totaldist + nports;

  ctr_routes_considered++;

  int wrong_final= final_isle && ports[nports-1] != final_isle;

  debugf("========== ROUTE");
  for (i=0; i<nports; i++)
    debugf(" %d",ports[i]);
  debugf("\n");

  double guess[AP]={0,0};
  if (nports>=2) {
    int pair[2], i;
    pair[1]= ports[nports-1];
    guess[A]= overestimate_excepting_tail;

    for (i=0; i<nports; i++) {
      pair[0]= ports[i];
      IslandPair *ip= ipair_get_maybe(pair[0], pair[1]);
      if (!ip) continue;
      if (ip->route_tail_value < 0) {
	ctr_subroute_tails_valued++;
	ip->route_tail_value= value_route(2, pair, pair[0]!=pair[1]);
      }
      guess[A] += ip->route_tail_value;
    }
    guess[P]= guess[A] / leagues_divisor;

    if (wrong_final) {
      ctr_routes_wrongfinalelim++;
      debugf(" WFELIM\n");
      return guess[A];
    }

    for (granui=0; granui<granus; granui++) {
      if (guess[A] > highscores[granui][A][0].value ||
	  guess[P] > highscores[granui][P][0].value)
	goto not_quickelim;
    }
    ctr_routes_quickelim++;
    debugf(" QELIM %f %f\n", guess[A], guess[P]);
    return guess[A];
  not_quickelim:;
  }

  int finisle= ports[nports-1];
  int finarch= isle2arch(finisle);

  int midisle= ports[nports/2];
  int midarch= route2midarch(ports,nports);

  Bucket *buckets[GRANUS];
  for (granui=0; granui<granus; granui++) {
    Bucket **buckets_fin;
    int mid, fin;
    switch (granui) {
    case 0: fin=finarch; mid=midarch; break;
    case 1: fin=finisle; mid=midarch; break;
    case 2: fin=finisle; mid=midisle; break;
    default: abort();
    }
    buckets_fin= ONDEMAND(buckets_base[granui][fin], granusz_mid[granui]);
    buckets[granui]= ONDEMAND(buckets_fin[mid], 1);
  }

  if (nports>=2) {
    for (granui=0; granui<granus; granui++)
      for (ap=0; ap<AP; ap++)
	if (guess[ap] > buckets[granui]->prs[ap].value[ap] &&
	    guess[ap] > highscores[granui][ap][0].value)
	  goto not_bucketelim;
    ctr_routes_bucketelim++;
    debugf(" ELIM %f %f\n", guess[A], guess[P]);
    return guess[A];
  not_bucketelim:
    debugf(" COMPUTE %f %f\n", guess[A], guess[P]);
  }

  ctr_routes_valued++;

  double value[AP];
  value[A]= value_route(nports, ports, 0);
  value[P]= value[A] / leagues_divisor;

  if (wrong_final) {
    ctr_routes_wrongfinal++;
    return value[0];
  }

  for (granui=granus-1; granui>=0; granui--) {
    Bucket *bucket= buckets[granui];

    if (value[A] <= bucket->prs[A].value[A] &&
	value[P] <= bucket->prs[P].value[P])
      continue;

    debugf(" SOMEHOW %d BEST\n",granui);

    fildebugf("granu %d f%d:%3d mid%d:%3d ",granui,
	      finarch,finisle,midarch,midisle);

    for (ap=0; ap<AP; ap++) {
      HighScoreEntry *scores= highscores[granui][ap];
      int nscores= nhighscores[granui][ap];

      fildebugf("ap=%d %15f", ap, value[ap]);
      if (value[ap] < bucket->prs[ap].value[ap]) {
	debugf("      ");
      } else {
	int pos;
	ctr_newbests_granu[granui*AP+ap]++;
	bucket->prs[ap].length= totaldist;
	memcpy(bucket->prs[ap].value, value, sizeof(value));
	memcpy(bucket->prs[ap].ports, ports, sizeof(*ports) * nports);
	if (nports < MAX_ROUTELEN-1) bucket->prs[ap].ports[nports]= -1;
	fildebugf("** ");
	for (pos=0; pos < nscores; pos++)
	  if (scores[pos].bucket == bucket) goto found;
	/* not found */
	pos= -1;
      found:
	for (;;) {
	  pos++;
	  if (pos >= nscores) break; /* new top */
	  if (scores[pos].value >= value[ap]) break; /* found spot */
	  if (pos>0)
	    scores[pos-1]= scores[pos];
	}
	pos--;
	if (pos>0) {
	  scores[pos].value= value[ap];
	  scores[pos].bucket= bucket;
	  if (granui < granus-1 &&
	      highscores[granui][A][0].bucket &&
	      highscores[granui][P][0].bucket) {
	    /* both absolute and perleague are full at this granularity,
	     * so we don't care about anything more granular */
	    fildebugf("\n                STOP-GRANU            ");
	    granus= granui+1;
	  }
	}
	fildebugf("@%2d/%2d ", pos, nscores);
      } /* new best */
    } /* ap */
  } /* granui */

  fildebugf(" route");

  for (i=0; i<nports; i++)
    fildebugf(" %d",ports[i]);
  fildebugf("\n");

  return value[0];
}

static void recurse(int last_isle,
		    int nports /* excluding last_isle */,
		    int totaldist /* including last_isle */,
		    double last_estimate) {
  ports[nports++]= last_isle;
  double estimate= process_route(nports, totaldist, last_estimate);
  if (nports >= MAX_ROUTELEN) return;

  Neighbour *add;
  for (add= get_neighbours(last_isle); add; add=add->next) {
    int newdist= totaldist + add->dist;
    if (newdist > max_dist) continue;

    recurse(add->islandid, nports, newdist, estimate);
  }
}

void search(int start_isle, int final_isle_spec,
	    Bucket ****buckets_base_io[GRANUS]) {
  int granui;
  for (granui=0; granui<GRANUS; granui++)
    buckets_base[granui]=
      ONDEMAND(*buckets_base_io[granui], granusz_fin[granui]);

  final_isle= final_isle_spec <= 0 ? 0 : final_isle_spec;
  recurse(start_isle,0,0,1e6);
}

int nhighscores[GRANUS][AP];
HighScoreEntry *highscores[GRANUS][AP];
int granus=GRANUS, granusz_fin[GRANUS], granusz_mid[GRANUS];

int narches;
char **archnames;
int *islandid2arch;

void setup_search(void) {
  MCALLOC(neighbours, islandtablesz);

  SQL_PREPARE(ss_neigh,
	      "SELECT biid, dist FROM routes WHERE aiid=?");

  int max_narches=
    sql_single_int(" SELECT count(*) FROM (\n"
		   "  SELECT DISTINCT archipelago\n"
		   "   FROM islands\n"
		   "  )");
  MCALLOC(archnames, max_narches);
  MCALLOC_INITEACH(islandid2arch, islandtablesz, *this=-1);

  sqlite3_stmt *archs;
  SQL_PREPARE(archs,
	      " SELECT islandid, archipelago\n"
	      "  FROM islands\n"
	      "  ORDER BY archipelago");
  while (SQL_STEP(archs)) {
    int isle= sqlite3_column_int(archs,0);
    const char *archname= (const char*)sqlite3_column_text(archs,1);
    int arch;
    for (arch=0; arch<narches; arch++)
      if (!strcmp(archnames[arch], archname)) goto found;
    assert(narches < max_narches);
    arch= narches++;
    archnames[arch]= masprintf("%s",archname);
  found:
    islandid2arch[isle]= arch;
  }
  sqlite3_finalize(archs);

  granusz_fin[0]=                granusz_mid[0]= narches;
  granusz_fin[1]= islandtablesz; granusz_mid[1]= narches;
  granusz_fin[2]=                granusz_mid[2]= islandtablesz;
}
