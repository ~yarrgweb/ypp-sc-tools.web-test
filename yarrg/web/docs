<%doc>

 This is part of the YARRG website.  YARRG is a tool and website
 for assisting players of Yohoho Puzzle Pirates.

 Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 Copyright (C) 2009 Clare Boothby

  YARRG's client code etc. is covered by the ordinary GNU GPL (v3 or later).
  The YARRG website is covered by the GNU Affero GPL v3 or later, which
   basically means that every installation of the website will let you
   download the source.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 are used without permission.  This program is not endorsed or
 sponsored by Three Rings.


 This Mason component generates the documentation.


</%doc>
<& docshead &>
<h1>Looking up data in YARRG</h1>

YARRG (Yet Another Revenue Research Gatherer) is a third-party tool
for helping find profitable trades and trade routes in Yohoho Puzzle
Pirates.  See the <a href="intro">Introduction</a> for more details.

<p>

The <a href="lookup">Market prices database</a> is the main output
from YARRG.  It offers a variety of enquiry options.

<p>

Hopefully you will be able to work it without too much help, but this
documentation page contains information about the database website
which you may not be able to divine from the online user interface.

<p>

If this page is all too complicated for you, you may prefer to read
<a href="intro">the introduction</a> instead.

<h2>Bookmarkable URLs</h2>

Mostly, you can bookmark the specific pages and queries.  Select the
ocean, query page, and other combinations of options, as you wish, and
perhaps fill in the actual data fields too, and bookmark the resulting
URL.

<p>

(An exception to this is if you select the "Update" option from the
"Trades for route" lookup; the list of (de)selected stalls is too long
to fit in a URL.)

<h2>Dynamic confirmation of meaning of text entry boxes</h2>

If you have Javascript enabled, the various text entry boxes will be
annotated with a brief explanation of the system's interpretation of
your current entry string.  To get the actual results updated, you
must still hit "Go" or "Update".

<h2>Trades for route</h2>

Given a list of islands (or archipelagoes), provides a list of
potentially profitable trades.  If the route is suitable for the trade
route optimiser, it will generate a complete voyage plan, telling you
which goods to buy and sell where at which stalls and prices.

<p><a name="arbitrage"></a>

If you specify only one island or one archipelago, the site shows only
arbitrage trades.  If you want single-hop trades within an
archipelago, you must enter the archipelago name twice.

<p>

After getting the results, you can untick various trades individually,
and select "Update" to get a new plan.  The unticked trades will be
excluded from the voyage plan (if any) and also from the totals.

<h3><a name="capacity">Vessel capacity</a></h3>

If you don't specify a vessel or a vessel capacity, the trading plan
will not take into account the fact that your voyage will be on a ship
with a limited size.  This will probably result in a plan
which trades excessively cumbersome goods (eg. hemp, wood, iron).

<p>

So you should specify your vessel capacity.  You can enter things
like:
<dl>
<dt>sloop
<dd>The capacity of a sloop, leaving no allowance for rum and shot
<dt>wb - 1%
<dd>The capacity of a war brig minus 1%
<dt>13t 20kl
<dd>13 tonnes (13,000kg), 20 kilolitres (20,000l)
<dt>sloop - 10 small 40 rum
<dd>The capacity of a sloop which remains after
    10 small shot and 40 rum are loaded
<dt>2t plus 500kg minus 200kg
<dd>2300kg, with no limit on volume
</dl>
Evaluation is strictly from left to right.

<p>

More formally:
<pre>
 capacity-string := [ first-term term* ]
 term := ('+' | '-' | 'plus' | 'minus') (value+ | number'%')
 value := mass | volume
        | integer commodity-name-or-abbreviation
 mass := number ('t' | 'kg')
 volume := number ('kl' | 'l')
 first-term := mass | volume | mass volume | volume mass
             | ship-name-or-abbreviation
</pre>

If the first term specifies only one of mass or volume, all the
subsequent terms may only adjust that same value.

<h3><a name="losses">Expected losses</a></h3>

In theory if you were guaranteed to have a trouble-free voyage it
would be worth trading goods at very low margins.  However, in
practice problems can arise: you may be attacked and lose your stock,
or market conditions may change between your collection and delivery
of the goods.

<p>

We model this by pretending that you expect to lose a fixed proportion
of your stock each league you sail.  This expected loss does not
appear in the trade tables (although the distance does), but it does
affect the way the voyage trading plan optimiser chooses which trades
to do.

<p>

Trades whose margin is less than the expected loss are never included
in the suggested plan.  For example, if you select 1% loss per league,
and plan a voyage of 5 leagues, then any trade with a margin of less
than 5.15% would be completely excluded (5.15% not 5% because the loss
works like compound interest).  Theoretically very profitable trades
which are close to the expected break-even point because of the
distance can also be rejected by the optimiser in favour of shorter
distance trades with theoretically smaller margins, if it's not
possible to do both.

<p>

As a guide: you may expect to lose between 0.01% and 1% per league.
For example 0.1% would correspond to losing one fight to brigands (who
take 10% if they win) for every 100 leagues sailed.

<p>

You can enter the value in the box either as a percentage, or as a
fraction 1/<em>divisor</em>, eg 1/2000 is the same as 0.05%; in each
case it is taken as the loss for each league of the voyage.

<h3><a name="minprofit">Minimum trade value</a></h3>

Often there are many low-volume, low-value trades.  It can be rather
labour-intensive to buy and sell a dozen different commodities, half
of which are only making a few poe each.

<p>

The "minimum trade value" specifies a minimum profit that you would
like to get from each (commodity, collect island, deliver island)
triplet.  Trades which don't meet this minimum will start out unticked in
the "Relevant trades" table and will not be included in the voyage
trading plan.

<p>

If you want to change your threshold, you have to select "Apply",
which will automatically tick and untick all of the tickboxes for in
"Relevant trades", as appropriate.  This will undo any customisation
of the set of trades you have already done by manually ticking and
unticking individual trades.

<p>

The value is an absolute poe amount, typically 5 or 10, representing
the minimum profit to make it worthwhile (from a time and effort point
of view) clicking in the YPP client to collect and deliver a
commodity.  Setting a higher threshold will make each island visit
faster, by excluding trivial transactions, and so reduce the chance
that market conditions change adversely during your voyage.

<h3><a name="poelimit">Caution about stalls' poe reserves</a></h3>

If you select <b>Also be cautious about stalls' poe reserves</b>,
YARRG will calculate a minimum amount of poe that each stall has on
hand (by looking at all the offers that stall is making), and never
plan for you to sell goods at that stall for more than the available
poe.

<p>

Goods planned to be bought at the stall (which might boost the stall's
poe reserves) are not considered, to avoid having to calculate the
stall's cash reserves at various different times.

<h3><a name="capital">Available capital</a></h3>

If you don't specify the amount of capital you have available to
invest in the voyage, the trading plan will assume that your capital
is unlimited.  If you specify an amount in PoE here, the trading plan
will never require you to spend more than that amount on commodities.

<p>

The trading plan does not take into account accumulated profits from
each leg of the journey when applying the available capital
constraint.  For example, if you specify a journey from A to B to C
and a capital limit of 10000 PoE, the trading plan will not tell you
to buy 1000 peas at A for 10 PoE each, sail them to B and sell all of
them for 20 PoE each, and then buy 2000 beans at B for 10 PoE each and
sail them to C to sell for 20 PoE each even if such a trade would in
fact be possible.  In practice this is unlikely to be a problem!

<h3><a name="posinclass">Locating commodities in the YPP client UI</a></h3>

In the Voyage Trading Plan, YARRG indicates after the commodity name
where in the YPP commodity UI each commodity can be found.  First
comes the initial letter of the category:
%     my $dbh= dbw_connect('Cerulean');
%     my $getclasses= $dbh->prepare(
%        "SELECT commodclass FROM commodclasses ORDER BY commodclass");
%     $getclasses->execute();
<%
    join '; ', map { $_->[0] =~ m/^./ or die; "<strong>$&</strong>$'" }
        @{ $getclasses->fetchall_arrayref() }
%>.
<p>

Then, if applicable, follows a number from <strong>0</strong> to
<strong>9</strong> indicating roughly where the commodity is in the
list of commodities of the same class.  The number indicates which
tenth of the list is: <strong>0</strong> for the first (top) tenth,
<strong>1</strong> for the 2nd, and so on, up to <strong>9</strong>
for the final tenth.

<p>
For example,
<blockquote>
<table><tr>
<td>Fine pink cloth&nbsp;&nbsp;
<td><div class=mouseover
 title="Fine pink cloth is under Cloth, commodity 14 of 55">C 2</div>
</table>
</blockquote>
indicates that Fine pink cloth can be found under Cloth,
between 20% and 30% of the way down through the types of Cloth.
If you mouseover that in a suitably equipped browser you should see the
text:
<blockquote>
Fine pink cloth is under Cloth, commodity 14 of 55
</blockquote>
<p>

The position indicator digit isn't shown for very small
categories.

The exact location of the commodity in the actual game
client may vary because YARRG only considers the list of all possible
commodities, not the list of actual offers at the island in question.

</div>
<& footer &>
