/*
 * Image canonicalisation function for use by callers feeding
 *  images into structure.c's routines.
 */
/*
 *  This is part of ypp-sc-tools, a set of third-party tools for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#ifndef STRUCTURE_H
#define STRUCTURE_H


#include "convert.h"


typedef struct { char blue2[256]; } CanonColourInfoBlues;
typedef struct { CanonColourInfoBlues *green2[256]; } CanonColourInfoGreens;
typedef struct { CanonColourInfoGreens *red2[256]; } CanonColourInfoReds;
extern CanonColourInfoReds canoncolourinfo_tree;

CanonImage *alloc_canon_image(int w, int h);

static inline char canon_lookup_colour(unsigned char r,
				       unsigned char g,
				       unsigned char b) {
  CanonColourInfoGreens *greens= canoncolourinfo_tree.red2[r];
  if (!greens) return '?';
  CanonColourInfoBlues *blues= greens->green2[g];
  if (!blues) return '?';
  return blues->blue2[b];
}

#define CANONICALISE_IMAGE(im,w,h,rgb_save, COMPUTE_RGB) do{		\
    /* compute_rgb should be a number of statements, or			\
     * a block, which assigns to					\
     *   Rgb rgb;							\
     * given the values of						\
     *   int x,y;							\
     * all of which are anamorphic.  Result is stored in im.		\
     * The COMPUTE_RGB is executed exactly once for			\
     * each pixel in reading order.					\
     */									\
    (im)= alloc_canon_image((w), (h));					\
    (rgb_save)= alloc_rgb_image((w), (h));				\
									\
    int x,y;								\
    for (y=0; y<(h); y++) {						\
      for (x=0; x<(w); x++) {						\
        Rgb rgb;							\
	COMPUTE_RGB;							\
        CANONIMG_ALSO_STORERGB((rgb_save));				\
	(im)->d[y*(w) + x]= canon_lookup_colour(rgb, rgb>>8, rgb>>16);	\
      }									\
      if (DEBUGP(rect)) {						\
	fprintf(debug, "%4d ",y);					\
	fwrite(im->d + y*(w), 1,(w), debug);				\
	fputc('\n',debug);						\
      }									\
    }									\
    debug_flush();							\
  }while(0)


#define CANONIMG_ALSO_STORERGB(ri)		\
  do{						\
    Rgb *rip= RI_PIXEL32((ri),x,y);		\
    *rip= rgb;					\
  }while(0)


#endif /*STRUCTURE_H*/
