/*
 * useful common stuff, mostly error handling and debugging
 */
/*
 *  This is part of ypp-sc-tools, a set of third-party tools for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#ifndef COMMON_H
#define COMMON_H

#define _GNU_SOURCE

#include <locale.h>
#include <stdarg.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <dirent.h>
#include <inttypes.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

typedef struct {
  int w,h;
  char d[];
} CanonImage;


/*----- debugging arrangements, rather contingent -----*/

typedef struct {
  int x, y;
} Point;

typedef struct { /* both inclusive */
  Point tl;
  Point br;
} Rect;

#define RECT_W(r) ((r).br.x - (r).tl.x + 1)
#define RECT_H(r) ((r).br.y - (r).tl.y + 1)

#ifdef DEBUG_FLAG_LIST
enum {
#define DF(f) dbg__shift_##f,
  DEBUG_FLAG_LIST
#undef DF
};
enum {
#define DF(f) dbg_##f = 1 << dbg__shift_##f,
  DEBUG_FLAG_LIST
#undef DF
};
#define DEBUGP(f) (!!(debug_flags & dbg_##f))

#endif /*DEBUG_FLAG_LIST*/

#ifndef debug_flags
extern unsigned debug_flags;
#endif

void debug_flush(void);
#ifndef debug
# define debug stderr
#endif

#define FMT(f,a) __attribute__((format(printf,f,a)))
#define SCANFMT(f,a) __attribute__((format(scanf,f,a)))
#define NORET __attribute__((noreturn))

#define DEFINE_VWRAPPERF(decls, funcf, otherattribs)			\
  decls void funcf(const char *fmt, ...) FMT(1,2) otherattribs;         \
  decls void funcf(const char *fmt, ...) {				\
    va_list al;  va_start(al,fmt);  v##funcf(fmt,al);  va_end(al);	\
  }

#define DEBUG_DEFINE_SOME_DEBUGF(fl,funcf)				\
  static void v##funcf(const char *fmt, va_list al) {			\
    if (DEBUGP(fl))							\
      vfprintf(debug,fmt,al);						\
  }									\
  DEFINE_VWRAPPERF(static, funcf, )

#define DEBUG_DEFINE_DEBUGF(fl) DEBUG_DEFINE_SOME_DEBUGF(fl,debugf)


/*---------- error handling ----------*/

extern int o_quiet;

void vwarning(const char *fmt, va_list) FMT(1,0);
void warning(const char *fmt, ...)      FMT(1,2);

void vprogress(const char *fmt, va_list) FMT(1,0);
void progress(const char *fmt, ...)      FMT(1,2);

void vprogress_log(const char *fmt, va_list) FMT(1,0);
void progress_log(const char *fmt, ...)      FMT(1,2);

void vprogress_spinner(const char *fmt, va_list) FMT(1,0);
void progress_spinner(const char *fmt, ...)      FMT(1,2);

void vfatal(const char *fmt, va_list)  FMT(1,0) NORET;
void fatal(const char *fmt, ...)       FMT(1,2) NORET;

#define sysassert(what) \
  ((what) ? (void)0 : sysassert_fail(__FILE__, __LINE__, #what))

void sysassert_fail(const char *file, int line, const char *what)
   __attribute__((noreturn));

void waitpid_check_exitstatus(pid_t pid, const char *what, int sigpipeok);


void *mmalloc(size_t sz);
void *mcalloc(size_t sz);
void *mrealloc(void *p, size_t sz);

char *masprintf(const char *fmt, ...) FMT(1,2);


#define ARRAYSIZE(a) ((sizeof((a)) / sizeof((a)[0])))
#define FILLZERO(obj) (memset(&(obj),0,sizeof((obj))))

#define STRING2(x) #x
#define STRING(x) STRING2(x)

#endif /*COMMON_H*/
